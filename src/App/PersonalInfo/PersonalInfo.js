import React from 'react';
import './PersonalInfo.css';

const PersonalInfo = () => (
	<div className="personal-info">
		<div className="photo">
      
	  	</div>
		<div className="cyte">
        	The European languages are members of the same family. Their separate existence is a myth.
   		</div>
		<div className="social-media-share">
			<a href="https://facebook.com"><img src="images/icons/fb-coloured.png" alt="facebook"></img></a>
			<a href="https://twitter.com"><img src="images/icons/tw-coloured.png" alt="twitter"></img></a>
			<a href="https://www.google.com"><img src="images/icons/g+coloured.png" alt="google+"></img></a>
			<a href="https://www.linkedin.com"><img src="images/icons/ln-coloured.png" alt="linkedin"></img></a>
		</div>
	</div>
)

export default PersonalInfo