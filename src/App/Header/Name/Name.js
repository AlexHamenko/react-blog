import React from 'react'
import './name.css';

const Name = () => (
	<div className="name-block">
		Tetyana Romanyuk
	</div>
)

export default Name