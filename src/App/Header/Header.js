import React from 'react'
import './Header.css';

import Name from './Name/Name'
import Menu from './Menu/Menu'


const Header = () => (
    <header className="header">
        <div className="header-overlay">
            <div className="header-wrapper">
                <Menu />
                <Name />
            </div>
        </div>
    </header>
)

export default Header

