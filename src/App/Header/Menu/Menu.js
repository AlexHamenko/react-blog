import React, {Component} from 'react'
import './menu.css';

class Menu  extends Component {

	state = {
		isMenuButtonOn: false,
	}

	render() {
		const isOn = this.state.isMenuButtonOn;
		return (
			<div className= "menu-block" onClick={ () => this.setState({isMenuButtonOn: !isOn})}>
				<a href="#" 
				className={isOn ? "menu-btn menu-btn_active" : "menu-btn" }>
					<span></span>
				</a>

				<nav 
				className={isOn ? "menu-nav menu-nav_active" : "menu-nav" }>
					<a href="#">Main</a>
					<a href="#">Porfolio</a>
					<a href="#">About</a>
					<a href="#">Contacts</a>
				</nav>
			</div>
		)
	}

}


export default Menu