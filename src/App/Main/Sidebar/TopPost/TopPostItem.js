import React,{ Component } from 'react'
import PropTypes from 'prop-types'

class TopPostItem extends Component {

    static propTypes = {
        title: PropTypes.string.isRequired,
        text:PropTypes.string.isRequired,
        tag:PropTypes.string,
    }

    render() {
        const {
            id,
            image,
            tag,
            data,
            title,
            text,
        } = this.props

        return (
            <div className="top top1">
                <div className="image-wrapper">
                    <img src={image} alt="top-post-1"/>
                </div>
                <div className="additional-info">
                    <div className="tag">{tag}</div>
                    <div className="data">{data}</div>
                </div>
                <h3>{title}</h3>
          </div>
        )
    }


}

export default TopPostItem