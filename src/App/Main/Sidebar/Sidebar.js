import React from 'react'
import './sidebar.css';

import Search from './Search/Search'
import Subscribe from './Subscribe/Subscribe'
import Tag from './Tag/Tag'
import TopPostList from './TopPost/TopPostList'


const Sidebar = () => (
    <aside className="sidebar">
        <Search />
        <TopPostList />
        <Tag />
        <Subscribe />

    </aside>
)

export default Sidebar