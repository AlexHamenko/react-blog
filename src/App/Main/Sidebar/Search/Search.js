import React from 'react'
import './search.css';

const Search = () => (
	<section className="search">
		<input type="search" placeholder="Search name here" name="search"/>
		<button type="button"></button>
  	</section>
)

export default Search