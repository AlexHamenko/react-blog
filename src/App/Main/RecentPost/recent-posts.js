const recentPosts = [
    {
        id: 1,
        image: "images/photos/post-1.jpg",
        tag: "Inspiration",
        data: "05 Jan 2019",
        title: "Post-1 Lorem ipsum dolor sit amet, consectetuer",
        text: "Post-1 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean condimentum ornare diam et venenatis. Sed enim tortor, dignissim ac iaculis vel, varius in velit. Curabitur ut lectus sed nisl mattis auctor. Aliquam cursus purus at imperdiet posuere. Curabitur scelerisque sagittis dui eu lobortis. Phasellus cursus dui nisi, non imperdiet augue gravida id. Morbi sagittis blandit dolor, at scelerisque diam. Mauris scelerisque leo varius bibendum volutpat. Vestibulum eu iaculis magna. Aenean aliquet purus eget lectus pharetra condimentum ut ut urna. Ut ultrices mattis tellus, eget fermentum nulla iaculis eget. Vivamus ut mollis nulla, pretium sagittis nibh. Donec consequat diam quis ante porttitor, vel efficitur risus tincidunt. Proin ac lobortis risus. Fusce ipsum leo, rhoncus id nunc ut, dignissim condimentum eros. Nam in rutrum orci, quis imperdiet dolor. Sed odio nunc, sodales id ligula quis, condimentum dignissim risus.",
        views: 145,
    },
    {
        id: 2,
        image: "images/photos/post-2.jpg",
        tag: "Inspiration",
        data: "05 Jan 2019",
        title: "Post-2 Lorem ipsum dolor sit amet, consectetuer",
        text: "Post-2 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean condimentum ornare diam et venenatis. Sed enim tortor, dignissim ac iaculis vel, varius in velit. Curabitur ut lectus sed nisl mattis auctor. Aliquam cursus purus at imperdiet posuere. Curabitur scelerisque sagittis dui eu lobortis. Phasellus cursus dui nisi, non imperdiet augue gravida id. Morbi sagittis blandit dolor, at scelerisque diam. Mauris scelerisque leo varius bibendum volutpat. Vestibulum eu iaculis magna. Aenean aliquet purus eget lectus pharetra condimentum ut ut urna. Ut ultrices mattis tellus, eget fermentum nulla iaculis eget. Vivamus ut mollis nulla, pretium sagittis nibh. Donec consequat diam quis ante porttitor, vel efficitur risus tincidunt. Proin ac lobortis risus. Fusce ipsum leo, rhoncus id nunc ut, dignissim condimentum eros. Nam in rutrum orci, quis imperdiet dolor. Sed odio nunc, sodales id ligula quis, condimentum dignissim risus.",
        views: 43
    },
    {
        id: 3,
        image: "images/photos/post-3.jpg",
        tag: "Inspiration",
        data: "05 Jan 2019",
        title: "Post-3 Lorem ipsum dolor sit amet, consectetuer",
        text: "Post-3 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean condimentum ornare diam et venenatis. Sed enim tortor, dignissim ac iaculis vel, varius in velit. Curabitur ut lectus sed nisl mattis auctor. Aliquam cursus purus at imperdiet posuere. Curabitur scelerisque sagittis dui eu lobortis. Phasellus cursus dui nisi, non imperdiet augue gravida id. Morbi sagittis blandit dolor, at scelerisque diam. Mauris scelerisque leo varius bibendum volutpat. Vestibulum eu iaculis magna. Aenean aliquet purus eget lectus pharetra condimentum ut ut urna. Ut ultrices mattis tellus, eget fermentum nulla iaculis eget. Vivamus ut mollis nulla, pretium sagittis nibh. Donec consequat diam quis ante porttitor, vel efficitur risus tincidunt. Proin ac lobortis risus. Fusce ipsum leo, rhoncus id nunc ut, dignissim condimentum eros. Nam in rutrum orci, quis imperdiet dolor. Sed odio nunc, sodales id ligula quis, condimentum dignissim risus.",
        views: 223,
    },
]

export default recentPosts