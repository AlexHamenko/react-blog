import React,{ Component } from 'react'
import PropTypes from 'prop-types'

class RecentPostItem extends Component {

    static propTypes = {
        title: PropTypes.string.isRequired,
        text:PropTypes.string.isRequired,
        tag:PropTypes.string,
    }

    render() {
        const {
            id,
            image,
            tag,
            data,
            title,
            text,
        } = this.props

        return (
            <article className="post post-1">
                <div className="image-wrapper">
                    <img src={image} alt="post"/>
                </div>
                <div className="additional-info">
                    <div className="tag">{tag}</div>
                    <div className="data">{data}</div>
                </div>
                <div className="text">
                    <h3>{title}</h3>
                    <p>{text}</p>
                </div>
                <div className="article-footer">
                    <a href="#" className="read-more-button">Read More</a>
                    <div className="share">
                    <p>Share</p>
                    <a href="https://facebook.com"><img src="images/icons/fb-dark.png" alt="facebook"/></a>
                    <a href="https://twitter.com"><img src="images/icons/tw-dark.png" alt="twitter"/></a>
                    <a href="https://www.google.com"><img src="images/icons/g+dark.png" alt="google+"/></a>
                    </div>
                </div>
            </article>
        )
    }


}

export default RecentPostItem