import React from 'react'
import './main.css';

import RecentPostList from './RecentPost/RecentPostList'
import Sidebar from './Sidebar/Sidebar'


const Main = () => (
    <main className="main">
        <div className="main-wrapper">
            <RecentPostList />
            <Sidebar />
        </div>
    </main>
)

export default Main

